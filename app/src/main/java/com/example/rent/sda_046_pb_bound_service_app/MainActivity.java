package com.example.rent.sda_046_pb_bound_service_app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements TimerService.TimerServiceListener {

    TimerService timerService;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            timerService = ((TimerService.TimerBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            timerService = null;
        }
    };

    @BindView(R.id.edit_text_input_number)
    protected EditText editTextInputNumber;

    @BindView(R.id.text_view_time_left)
    protected TextView textViewTimeLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // tu trzeba zbindowac serwice a odpinamy w on destroy

        Intent intent = new Intent(this, TimerService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

    }

    @OnClick(R.id.button_start)
    protected void clickStartButton() {
        String time = editTextInputNumber.getText().toString();
        // ta aktywnosc trzeba wziaz jako this zamiast samego listenera
        timerService.startCounting(time, this);

    }


    @Override
    public void onConterUpdate(int seconds) {
        textViewTimeLeft.setText(String.valueOf(seconds));
    }

    @Override
    public void onCounterFinish(String finishText) {
        textViewTimeLeft.setText(finishText);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
