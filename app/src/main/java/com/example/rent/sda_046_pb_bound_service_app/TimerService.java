package com.example.rent.sda_046_pb_bound_service_app;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by RENT on 2017-06-13.
 */

public class TimerService extends Service {

    private IBinder binder = new TimerBinder();
    // nie skonczylem zabawy z ta flaga
    private boolean paused = true;
    CountDownTimer countDownTimer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public boolean startCounting(String time, final TimerServiceListener listener) {



        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        long counterValue = Long.parseLong(time) * 1000;
        countDownTimer = new CountDownTimer(counterValue, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                String counter = String.valueOf(millisUntilFinished / 1000);
                int seconds = Integer.parseInt(counter);

                listener.onConterUpdate(seconds);
            }

            @Override
            public void onFinish() {
                listener.onCounterFinish("Time is up!");


            }
        }.start();

        return true;
    }

    public boolean cancelCounting() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        return true;
    }


    public class TimerBinder extends Binder {
        public TimerService getService() {
            return TimerService.this;
        }

    }

    interface TimerServiceListener {
        void onConterUpdate(int seconds);
        void onCounterFinish(String finishText);


    }
}
